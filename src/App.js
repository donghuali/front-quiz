import React from 'react';
import './App.css';
import {BrowserRouter as Router} from "react-router-dom";
import Home from "./pages/Home";
import {Route, Switch} from "react-router";
import AddProduct from "./pages/Add";
import Order from "./pages/Order/idnex";
import store from "./store";
import {Provider} from "react-redux"
function App() {
  return (
    <Provider store={store}>
        <Router>
            <Switch>
                <Route exact path={"/"} component={Home}></Route>
                <Route path={"/add"} component={AddProduct}></Route>
                <Route path={"/order"} component={Order}></Route>
            </Switch>
        </Router>
    </Provider>
  );
}

export default App;
