import React,{Component} from "react"
import Header from "../../component/Header";
import "./index.less"
import axios from "axios";
class AddProduct extends Component {
    constructor(props) {
        super(props)
        this.state={
            name:'',
            price:"",
            unit:"",
            img:''
        }
        this.handleAdd = this.handleAdd.bind(this);
        this.changeName = this.changeName.bind(this);
        this.changePrice = this.changePrice.bind(this);
        this.changeUnit = this.changeUnit.bind(this);
        this.changeImg = this.changeImg.bind(this);
    }
    componentDidMount() {
    }

    changeName(e){
        this.setState({
            name:e.target.value
        })
    }
    changePrice(e){
        this.setState({
            price:e.target.value
        })
    }
    changeUnit(e){
        this.setState({
            unit:e.target.value
        })
    }
    changeImg(e){
        this.setState({
            img:e.target.value
        })
    }
    handleAdd(event){
        event.preventDefault();
        let object = {
            name:this.state.name,
            price:this.state.price,
            unit:this.state.unit,
            img:this.state.img
        }
        axios({
            method:"post",
            url:"http://localhost:8090/api/product",
            headers: { 'content-type': 'application/json;charset=UTF-8' },
            data: JSON.stringify(object)

        }).then(res=>{
            if (res.status == 200) {
                window.location.href="/"
            }
        }).catch(error=>error);

    }
    render() {
        return (
            <div>
                <Header></Header>
                <h2>添加商品</h2>
                <main className={"main"}>
                    <form onSubmit={this.handleAdd} >
                        <label htmlFor="name">*名称:</label>
                        <input required placeholder={"名称"} onChange={this.changeName} id={"name"} name={"name"}/>
                        <label htmlFor={"price"}>*价格:</label>
                        <input type={"tel"} required placeholder={"价格"} onChange={this.changePrice} id={"price"} name={"price"}/>
                        <label htmlFor={"unit"}>*单位:</label>
                        <input required placeholder={"单位"} onChange={this.changeUnit} id={"unit"} name={"unit"}/>
                        <label htmlFor={"img"}>*图片:</label>
                        <input required placeholder={"图片"} onChange={this.changeImg} id={"img"} name={"img"}/>
                        <button className={"submit"}>提交</button>
                    </form>
                </main>

            </div>
        );
    }
}



export default AddProduct;
