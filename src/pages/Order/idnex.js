import React, { Component } from "react"
import Header from "../../component/Header";
import { Table, Divider, Tag } from 'antd';
import "./style.less"
import 'antd/dist/antd.css';
import axios from "axios";
import { Link } from "react-router-dom";
class Order extends Component {
    constructor(props) {
        super(props)
        this.state = {
            order: []
        };
    }

    delete(data) {
        axios({
            method: "delete",
            url: "http://localhost:8090/api/delete/order_item",
            headers: { "content-type": "application/json" },
            data: JSON.stringify(data)
        }).then(res => {
            window.location.reload();
        })
    }
    componentDidMount() {
        axios({
            method: "get",
            url: "http://localhost:8090/api/order_item",
            headers: { "content-type": "application/json" },
        }).then(res => {
            console.log(res)
            this.setState({
                order: res.data
            })
        })
    }
    render() {


        const columns = [
            {
                title: '名字',
                dataIndex: 'product.name',
                key: 'product.name',
                render: name => <span>{name}</span>
            },
            {
                title: '单价',
                dataIndex: 'product.price',
                key: 'product.price',
            },
            {
                title: '数量',
                dataIndex: 'count',
                key: 'count',
            },
            {
                title: '单位',
                key: 'product.unit',
                dataIndex: 'product.unit',
                render: unit => <span>{unit}</span>
            },
            {
                title: '操作',
                dataIndex: "id",
                render: (dataIndex) => (
                    <span>
                        <button onClick={() => { this.delete(dataIndex) }} className='delete'>删除</ button>
                    </span>
                ),
            },
        ];

        const data = this.state.order;
        return (
            <div className="order">
                <Header></Header>
                {this.state.order.length == 0 ? <span className="noOrder">暂无订单,
                <Link to="/">快去首页添加吧</Link>
                </span> : <Table className="table" columns={columns} dataSource={data} />}
            </div>
        )
    }
}
export default Order;
