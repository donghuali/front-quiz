
import React, { Component } from "react";
import Header from "../../component/Header";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getProduct } from "../../store/home/action-create";
import "./style.less"
import { Icon } from "antd";
import * as axios from "axios";
import { Spin } from 'antd';
import 'antd/dist/antd.css';
class Home extends Component {
    constructor(props) {
        super(props)
        this.addOrder = this.addOrder.bind(this);
    }

    addOrder(index) {
        axios({
            method: "post",
            url: "http://localhost:8090/api/create/order_item",
            headers: { "content-type": "application/json" },
            data: JSON.stringify(index)
        }).then(res => {
            console.log(res)
            if (res == 200) {
            }
        })
    }
    componentDidMount() {
        this.props.getProduct();
    }

    render() {
        const productList = this.props.product.product || [];
        return (
            <div className="home">
                <Header></Header>
                <ul>
                    {productList.map((item, index) => {
                        return <li key={index}>
                            <img src={item.img}></img>
                            <div className="name">{item.productName}</div>
                            <div className="price">单价:{item.price}元/瓶</div>
                            <Icon onClick={() => { this.addOrder(item.id) }} className="add" type="plus-circle" />
                        </li>
                    })}
                </ul>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    product: state.products
})
const mapDisPatchToProps = dispatch => bindActionCreators({
    getProduct,
}, dispatch)
export default connect(mapStateToProps, mapDisPatchToProps)(Home);
