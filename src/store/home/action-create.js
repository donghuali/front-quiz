import * as types from "./action-types"

export const getProduct = ()=>{
    return dispatch =>{
        fetch("http://localhost:8090/api/products")
            .then(res=>res.json())
            .then(res=>{
                dispatch({
                    type:types.OBTAIN_PRODUCT,
                    data:res
                })
            })
    }
}
