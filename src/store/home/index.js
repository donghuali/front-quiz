import * as types from "./action-types"
import { product } from "./default"
const reducer = (product = {}, action) => {
    switch (action.type) {
        case types.OBTAIN_PRODUCT:
            return {
                ...product,
                product: action.data
            }

        default: return product;
    }
}
export default reducer
