import React, { Component } from "react";

import "./index.less";
import { NavLink } from "react-router-dom";
class Header extends Component {
    constructor(props) {
        super(props);

    }


    render() {
        return (
            <div className='header'>
                <NavLink exact activeClassName="active" className={"link"} to="/">商城</NavLink>
                <NavLink activeClassName="active" className={"link"} to="/order">订单</NavLink>
                <NavLink activeClassName="active" className={"link"} to={"/add"}>添加商品</NavLink>
            </div>
        );
    }

}

export default Header;
